"use strict";

const switcherBtn = document.querySelector('.switcher');

function themeSet(themeName) {
    localStorage.setItem('theme', themeName);
    document.documentElement.className = themeName;
}

function themeToggle() {
    if (localStorage.getItem('theme') === 'theme-dark') {
        themeSet('theme-light');
    } else {
        themeSet('theme-dark');
    }
}

document.addEventListener("DOMContentLoaded", () => {
    document.documentElement.className = localStorage.getItem('theme');
});

switcherBtn.addEventListener('click', themeToggle);

